# JLP Back-end Technical Test - Dresses
Submitted by Pete Whelpton.  Evaluting the requirements, there appeared to be two potential approaches:

1. Modify the existing Mapper, Product classes etc. to meet the project brief
2. Create new Mapper + Product (e.g. DiscountProduct) etc. classes and perform a 2nd pass on the data already returned 

I chose option 1.

## Instructions
To run unit tests:
`./gradlew test`

To compile and start the web server:
`./gradlew clean build bootRun`

The discount dresses endpoint will then be served up at `localhost:8080/products/dresses`  
LabelType can be passed in as a query string parameter (one of `showwasnow`, `showwasthennow`, `showpercdiscount`), e.g.
`localhost:8080/products/dresses?labelType=showwasthennow`).

## Asumptions
- Prices returned by the internal API  will always start with £ sign, and be a decimal value e.g. `£19.50`
- Not to add additional library to project - using internal Java / existing Spring APIs
- Pounds value of Price will never overflow int datatype, so we can use int primitive rather than BigDecimal for calculations
- Percentage discount should be an integer, rounded down to avoid overstating discount

## Potential Improvements 
- Could use a dedicated, tested [Money API](https://github.com/JavaMoney/jsr354-api) for calculations
- If the project were updated from JDK11 to JDK17, the Lombok @data annotation replaced by Java Record class
- Mapper class could be abstract rather than Interface with default methods, so utils could be instantiated rather 
  than static  
- Project could use internationalization for currency and language strings (e.g. `Was £x.xx, now £y.yy` could be loaded
  from a `.properties` file)
- API key shouldn't be stored in `.properties` file. Ideally, there would be a placehold that was reaplced with the real
  API key (stored in secure credential management) during a CI/CD pipeline
  
## Example output
Output from `localhost:8080/products/dresses?labelType=showwasthennow`:
```json
[
    {
        "productId": "5727625",
        "title": "ANYDAY John Lewis & Partners Kids' Tiered Jersey Dress",
        "colorSwatches": [
            {
                "color": "Blue",
                "rgbColor": "0000FF",
                "skuId": "240005700"
            },
            {
                "color": "Brown",
                "rgbColor": "A52A2A",
                "skuId": "240005560"
            },
            {
                "color": "Grey",
                "rgbColor": "C2C5CC",
                "skuId": "240005561"
            }
        ],
        "nowPrice": "£4.50",
        "priceLabel": "Was £9.00, then £6.00, now £4.50"
    }
]
```