package com.johnlewis.betechtest.products.controller;

import com.johnlewis.betechtest.products.request.LabelType;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.service.DressesService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/products")
public class ProductsController {

    /**
     * Dresses service to get Dress information from JLP internal API
     */
    private final DressesService dressesService;
    /**
     * Default value for labelType query string parameter
     */
    private final LabelType DEFAULT_LABELTYPE;

    /**
     * Construct an instance of the ProductsController to handle HTTP requests to `/products` URLs
     *
     * @param dressesService   Dresses service to get Dress information from JLP internal API
     * @param defaultLabelType Default value for labelType query string parameter
     */
    @Autowired
    ProductsController(DressesService dressesService,
                       @Value("${jl.request.default.labeltype}") LabelType defaultLabelType) {
        this.dressesService = dressesService;
        this.DEFAULT_LABELTYPE = defaultLabelType;
    }

    /**
     * Handle HTTP requests to `/products/dresses`.  Get list of discounted dresses from the DressService and
     * return to client.  If no labelType parameter is provided, the default will be used.
     *
     * @param labelType query parameter `?labelType=` to determine format of LabelType attribute in response
     * @return Array of discount dress Products
     */
    @GetMapping(value = "/dresses", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Endpoint to obtain discounted dress information")
    public ResponseEntity<List<Product>> getDiscountDresses(@RequestParam("labelType") Optional<LabelType> labelType) {
        return ResponseEntity.ok(this.dressesService.getDiscountedDresses(labelType.orElse(DEFAULT_LABELTYPE)));
    }
}
