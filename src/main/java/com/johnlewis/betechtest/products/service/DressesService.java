package com.johnlewis.betechtest.products.service;

import com.johnlewis.betechtest.products.mapper.ProductMapper;
import com.johnlewis.betechtest.products.request.LabelType;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.response.ProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DressesService {
    private final String johnLewisDressApi;
    private final RestTemplate restTemplate;
    private final ProductMapper productMapper;

    @Autowired
    DressesService(RestTemplate restTemplate, @Value("${jl.api.key}") String apiKey) {
        this.restTemplate = restTemplate;
        this.productMapper = ProductMapper.INSTANCE;
        this.johnLewisDressApi = "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dresses&key=" + apiKey;
    }

    public List<Product> getDiscountedDresses(LabelType labelType) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
            headers.set("User-Agent", "Back-end tech test");
            HttpEntity<String> entity = new HttpEntity<>(headers);
            ProductResponse productResponse = restTemplate.exchange(
                    johnLewisDressApi,
                    HttpMethod.GET,
                    entity,
                    ProductResponse.class)
                    .getBody();
            return productResponse != null && productResponse.getProducts() != null
                    ? productResponse.getProducts()
                        .stream()
                        .filter(p -> p.getVariantPriceRange().getReductionHistory().size() > 0)
                        .collect(Collectors.toList())
                        .stream()
                        .map(p -> productMapper.toProduct(p, labelType))
                        .collect(Collectors.toList())
                    : new ArrayList<>();
        } catch (Exception e) {
            System.out.println(e);
        }
        return new ArrayList<>();
    }

}
