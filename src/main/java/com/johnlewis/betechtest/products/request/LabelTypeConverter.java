package com.johnlewis.betechtest.products.request;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Custom converter to convert labelType query string paramter to LabelType enum.
 */
@Component
public class LabelTypeConverter implements Converter<String, LabelType> {

    /**
     * Default LabelType as defined in application.properties.  Usually
     * SHOWWASNOW.
     */
    @Value("${jl.request.default.labeltype}")
    private LabelType DEFAULT_LABELTYPE;

    /**
     * Convert a query string parameter to LabelType enum.
     *
     * @param source labelType query string parameter
     * @return LabelType enum matching string parameter, or the default values
     */
    @Override
    public LabelType convert(String source) {
        try {
            return LabelType.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e) {
            return DEFAULT_LABELTYPE;
        }
    }
}
