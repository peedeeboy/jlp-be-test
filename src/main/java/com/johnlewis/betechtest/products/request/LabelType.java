package com.johnlewis.betechtest.products.request;

/**
 * Valid options for LabelType HTTP request query string parameter.
 */
public enum LabelType {
    /**
     * PriceLabel should be returned in format `Was £x.xx, now £y.yyy`
     */
    SHOWWASNOW,
    /**
     * PriceLabel should be returned in format `Was £x.xx, then £y.yy, now £z.zzz`
     * If there is no then value in price.then just return was and now as above.
     */
    SHOWWASTHENNOW,
    /**
     * PriceLabel should be returned in format `x% off - now £y.yy`
     */
    SHOWPERCDISCOUNT
}


