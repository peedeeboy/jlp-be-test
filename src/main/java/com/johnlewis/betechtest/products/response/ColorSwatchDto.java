package com.johnlewis.betechtest.products.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ColorSwatchDto {
    private String basicColor;
    private String skuId;
}
