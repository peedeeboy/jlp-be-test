package com.johnlewis.betechtest.products.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ColorSwatch {
    private String color;
    private String rgbColor;
    private String skuId;
}
