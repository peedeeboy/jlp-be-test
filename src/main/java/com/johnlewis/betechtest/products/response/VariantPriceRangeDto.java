package com.johnlewis.betechtest.products.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class VariantPriceRangeDto {
    private Display display;
    private List<ReductionHistoryDto> reductionHistory;
}
