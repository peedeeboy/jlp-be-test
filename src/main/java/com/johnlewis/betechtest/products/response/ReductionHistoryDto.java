package com.johnlewis.betechtest.products.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReductionHistoryDto {
    private int chronology;
    private Display display;
}
