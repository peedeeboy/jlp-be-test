package com.johnlewis.betechtest.products.util;

import com.johnlewis.betechtest.products.request.LabelType;

/**
 * Utility class to convert currency Strings to their display requirements.
 */
public class PriceStringFormatter {

    // These strings would ideally come from localization & a messages.proeprties file
    private static final String CURRENCY_SYMBOL = "£";
    private static final String CURRENCY_SEPERATOR_REGEX = "\\.";
    private static final String PERCENTAGE_REGEX = "[£]|[\\.]";
    private static final String PRICE_ZERO = "£0.00";

    private static final String SHOWWASNOW_LABEL = "Was %s, now %s";
    private static final String SHOWWASTHENNOW_LABEL = "Was %s, then %s, now %s";
    private static final String PERCDISCOUNT_LABEL = "%s off - now %s";

    /**
     * Format a Product price to the correct display foramt.  If a product costs less than £10.00, the price should
     * include the decimal pence.  If the price is >= £10.00, the display price should be the pounds value only.
     *
     * @param price string format representation of price (e.g. `£10.60`)
     * @return display format of price according to formatting rules
     */
    public static String formatPrice(String price) {
        if (price == null) {
            return PRICE_ZERO;
        }
        String[] splitPrice = price.split(CURRENCY_SYMBOL);
        String[] poundsPence = splitPrice[1].split(CURRENCY_SEPERATOR_REGEX);
        int pounds = Integer.parseInt(poundsPence[0]);

        return pounds < 10 ? price : CURRENCY_SYMBOL + poundsPence[0];
    }

    /**
     * Formats the priceLabel string according to formatting rules.
     *
     * @param labelType enum specifying which format should be used
     * @param was original price in string format (e.g. `£12.00`)
     * @param then first discounted price in string format (e.g. `£10.00`)
     * @param now current price in string format (e.g. `£7.50`)
     * @return priceLabel formatted according to labelType parameter
     */
    public static String formatLabelPrice(LabelType labelType, String was, String then, String now) {
        String labelPrice = "";
        switch(labelType) {
            case SHOWWASNOW:
                labelPrice = String.format(SHOWWASNOW_LABEL, formatPrice(was), formatPrice(now));
                break;
            case SHOWWASTHENNOW:
                labelPrice = then != null
                        ? String.format(SHOWWASTHENNOW_LABEL, formatPrice(was), formatPrice(then), formatPrice(now))
                        : String.format(SHOWWASNOW_LABEL, formatPrice(was), formatPrice(now));
                break;
            case SHOWPERCDISCOUNT:
                String percDiscount = calculatePercentageDiscount(was, now);
                labelPrice = String.format("%s off - now %s", percDiscount, formatPrice(now));
                break;
        }
        return labelPrice;
    }

    /**
     * Calculate the percentage discount between two prices.  If the current price is greater than
     * the original price (e.g. because the parameters were passed in the wrong way around), return
     * `0%` as the discount.
     *
     * @param was the original price in String format (e.g. `£9.00`)
     * @param now the current price in String form (e.g. `£4.50`)
     * @return formatted String of percentage discount (e.g. `50%`).
     */
    public static String calculatePercentageDiscount(String was, String now) {
        int wasPence = Integer.parseInt(was.replaceAll(PERCENTAGE_REGEX, ""));
        int nowPence = Integer.parseInt(now.replaceAll(PERCENTAGE_REGEX, ""));
        int percDiscount = nowPence > wasPence ? 0 : (wasPence - nowPence) * 100 / wasPence;

        return percDiscount + "%";
    }
}
