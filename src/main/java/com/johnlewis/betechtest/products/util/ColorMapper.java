package com.johnlewis.betechtest.products.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Utility class to covert basic color strings to their RGB hexidecimal value.
 */
public class ColorMapper {

    /**
     * Hashmap mapping a basic color string (e.g. `Blue`) to a hexidecimal string representation of the RGB value
     * (e.g. `0000FF`)
     */
    private static final Map<String, String> COLOR_MAP = new HashMap<>();

    static {
        COLOR_MAP.put("Black", "000000");
        COLOR_MAP.put("Blue", "0000FF");
        COLOR_MAP.put("Brown", "A52A2A");
        COLOR_MAP.put("Green", "00FF00");
        COLOR_MAP.put("Grey", "C2C5CC");
        COLOR_MAP.put("Neutrals", "F6EFD0");
        COLOR_MAP.put("Pink", "FFC0CB");
        COLOR_MAP.put("Purple", "800080");
        COLOR_MAP.put("Red", "FF0000");
        COLOR_MAP.put("White", "FFFFFF");
        COLOR_MAP.put("Yellow", "FFFF00");
    }

    /**
     * Get the hexidecimal RGB value for the given basic color String.
     *
     * @param basicColor String representation of basic color. e.g. `White`
     * @return Hexidecimal RGB string for the provided color (e.g. `FFFFFF`) or empty string if basic color parameter
     * not recognised.
     */
    public static String getRgbValueForColor(String basicColor) {
        return !COLOR_MAP.containsKey(basicColor) ? "" : COLOR_MAP.get(basicColor);
    }
}
