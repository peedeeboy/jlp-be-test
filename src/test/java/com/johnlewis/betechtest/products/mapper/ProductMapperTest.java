package com.johnlewis.betechtest.products.mapper;

import com.johnlewis.betechtest.products.request.LabelType;
import com.johnlewis.betechtest.products.response.ColorSwatch;
import com.johnlewis.betechtest.products.response.ColorSwatchDto;
import com.johnlewis.betechtest.products.response.Display;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.response.ProductDto;
import com.johnlewis.betechtest.products.response.ReductionHistoryDto;
import com.johnlewis.betechtest.products.response.VariantPriceRangeDto;
import com.johnlewis.betechtest.products.util.ColorMapper;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ProductMapperTest {

    @Test
    void whenFullValidProductDtoProvided_thenProductShouldBeMappedCorrectly() {

        String productId = "productId";
        String title = "title";

        VariantPriceRangeDto variantPriceRangeDto = createVariantPriceRangeDto();

        ColorSwatchDto colorSwatchDto = new ColorSwatchDto();
        colorSwatchDto.setBasicColor("Blue");
        colorSwatchDto.setSkuId("skuId");
        List<ColorSwatchDto> colorSwatchDtos = List.of(colorSwatchDto);

        ProductDto productDto = new ProductDto();
        productDto.setProductId(productId);
        productDto.setTitle(title);
        productDto.setColorSwatches(colorSwatchDtos);
        productDto.setVariantPriceRange(variantPriceRangeDto);

        Product product = new Product();
        product.setProductId(productId);
        product.setTitle(title);
        ColorSwatch colorSwatch = new ColorSwatch("Blue", ColorMapper.getRgbValueForColor("Blue"),
                "skuId");
        product.setColorSwatches(List.of(colorSwatch));
        product.setNowPrice("£8.00");
        product.setPriceLabel("Was £15, then £12, now £8.00");

        ProductMapper productMapper = ProductMapper.INSTANCE;
        Product result = productMapper.toProduct(productDto, LabelType.SHOWWASTHENNOW);

        assertEquals(product, result);
    }

    @Test
    void whenDtoVariantPriceRangeIsNull_thenProductPriceLabelShouldBeNull() {
        ProductMapper productMapper = ProductMapper.INSTANCE;
        assertNull(productMapper.toPriceLabel(null, LabelType.SHOWWASNOW));
    }

    @Test
    void whenDtoVarientPriceRangeIsValidAndLabelTypeIsShowWasNow_thenPriceLabelShouldBeMappedCorrectly() {
        VariantPriceRangeDto variantPriceRangeDto = createVariantPriceRangeDto();

        ProductMapper productMapper = ProductMapper.INSTANCE;
        assertEquals("Was £15, now £8.00", productMapper.toPriceLabel(variantPriceRangeDto,
                LabelType.SHOWWASNOW));

    }

    @Test
    void whenDtoVarientPriceRangeIsValidAndLabelTypeIsShowWasThenNow_thenPriceLabelShouldBeMappedCorrectly() {
        VariantPriceRangeDto variantPriceRangeDto = createVariantPriceRangeDto();

        ProductMapper productMapper = ProductMapper.INSTANCE;
        assertEquals("Was £15, then £12, now £8.00", productMapper.toPriceLabel(variantPriceRangeDto,
                LabelType.SHOWWASTHENNOW));

    }

    @Test
    void whenDtoVarientPriceRangeIsValidAndLabelTypeIsShowPercDiscount_thenPriceLabelShouldBeMappedCorrectly() {
        VariantPriceRangeDto variantPriceRangeDto = createVariantPriceRangeDto();

        ProductMapper productMapper = ProductMapper.INSTANCE;
        assertEquals("46% off - now £8.00", productMapper.toPriceLabel(variantPriceRangeDto,
                LabelType.SHOWPERCDISCOUNT));

    }

    @Test
    void whenDtoDisplayPriceIsNull_thenProductNowPriceShouldBeZero() {
        ProductMapper productMapper = ProductMapper.INSTANCE;
        assertEquals("£0.00", productMapper.toNowPrice(null));
    }

    @Test
    void whenDtoDisplayPriceIsValid_thenProductNowPriceShouldBeMapped() {
        VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        variantPriceRangeDto.setDisplay(new Display("£15.00", "£14.00"));

        ProductMapper productMapper = ProductMapper.INSTANCE;
        assertEquals("£15", productMapper.toNowPrice(variantPriceRangeDto.getDisplay()));
    }

    @Test
    void whenValidColorSwatchDtoIsProvided_thenColorSwatchShouldBeMapped() {
        ColorSwatchDto colorSwatchDto = new ColorSwatchDto();
        colorSwatchDto.setBasicColor("Blue");
        colorSwatchDto.setSkuId("skuId");

        ProductMapper productMapper = ProductMapper.INSTANCE;
        List<ColorSwatch> colorSwatches = productMapper.toColorSwatches(List.of(colorSwatchDto));
        assertEquals(1, colorSwatches.size());

        ColorSwatch colorSwatch = colorSwatches.get(0);
        assertEquals(colorSwatchDto.getBasicColor(), colorSwatch.getColor());
        assertEquals(ColorMapper.getRgbValueForColor(colorSwatchDto.getBasicColor()), colorSwatch.getRgbColor());
        assertEquals(colorSwatchDto.getSkuId(), colorSwatch.getSkuId());
    }

    @Test
    void shouldProvideNullResponseForEmptyObjectWhenMappingToProduct() {
        ProductMapper productMapper = ProductMapper.INSTANCE;
        assertNull(productMapper.toProduct(null, LabelType.SHOWWASNOW));
    }

    private VariantPriceRangeDto createVariantPriceRangeDto() {
        VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        variantPriceRangeDto.setDisplay(new Display("£8.00", "£6.00"));

        ReductionHistoryDto reductionHistory1 = new ReductionHistoryDto();
        reductionHistory1.setDisplay(new Display("£15.00", "£14.00"));
        reductionHistory1.setChronology(0);
        ReductionHistoryDto reductionHistory2 = new ReductionHistoryDto();
        reductionHistory2.setDisplay(new Display("£12.00", "£11.00"));
        reductionHistory2.setChronology(1);
        variantPriceRangeDto.setReductionHistory(List.of(reductionHistory1, reductionHistory2));

        return variantPriceRangeDto;
    }
}
