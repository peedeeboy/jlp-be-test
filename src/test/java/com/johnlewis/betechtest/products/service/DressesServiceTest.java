package com.johnlewis.betechtest.products.service;

import com.johnlewis.betechtest.products.request.LabelType;
import com.johnlewis.betechtest.products.response.ColorSwatchDto;
import com.johnlewis.betechtest.products.response.Display;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.response.ProductDto;
import com.johnlewis.betechtest.products.response.ProductResponse;
import com.johnlewis.betechtest.products.response.ReductionHistoryDto;
import com.johnlewis.betechtest.products.response.VariantPriceRangeDto;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DressesServiceTest {
    private static final String API_KEY = "xxxxxxxxx";

    private final RestTemplate restTemplate = mock(RestTemplate.class);
    private final DressesService dressesService = new DressesService(restTemplate, API_KEY);
    private final LabelType labelType = LabelType.SHOWWASNOW;

    @Test
    void shouldReturnEmptyListWhenExceptionThrown() {

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenThrow(RestClientException.class);

        List<Product> products = dressesService.getDiscountedDresses(labelType);
        assertNotNull(products);
        assertEquals(0, products.size());
    }

    @Test
    void shouldReturnEmptyListAsEmptyResponse() {

        ProductResponse productResponse = new ProductResponse();
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

        List<Product> products = dressesService.getDiscountedDresses(labelType);
        assertNotNull(products);
        assertEquals(0, products.size());
    }

    @Test
    void shouldReturnEmptyListAsProductListEmpty() {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProducts(new ArrayList<>());
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

        List<Product> products = dressesService.getDiscountedDresses(labelType);
        assertNotNull(products);
        assertEquals(0, products.size());
    }

    @Test
    void whenResponseContainsBothDiscountedAndFullPriceProducts_thenOnlyDiscountedProductsAreReturned() {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setProducts(getProductDtos());
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

        List<Product> products = dressesService.getDiscountedDresses(labelType);
        assertNotNull(products);
        assertEquals(1, products.size());
    }

    private List<ProductDto> getProductDtos() {
        String productId = "productId";
        String title = "title";
        VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        variantPriceRangeDto.setDisplay(new Display("£10.00", "£5.00"));
        ReductionHistoryDto reductionHistory = new ReductionHistoryDto();
        reductionHistory.setDisplay(new Display("£15.00", "£10.00"));
        reductionHistory.setChronology(0);
        variantPriceRangeDto.setReductionHistory(List.of(reductionHistory));
        ColorSwatchDto colorSwatchDto = new ColorSwatchDto();
        colorSwatchDto.setBasicColor("Blue");
        colorSwatchDto.setSkuId("skuId");
        List<ColorSwatchDto> colorSwatchDtos = List.of(colorSwatchDto);

        ProductDto discountedProductDto = new ProductDto();
        discountedProductDto.setProductId(productId);
        discountedProductDto.setTitle(title);
        discountedProductDto.setColorSwatches(colorSwatchDtos);
        discountedProductDto.setVariantPriceRange(variantPriceRangeDto);

        // Clear out reduction history to make a full price DTO
        VariantPriceRangeDto variantPriceRangeDto2 = new VariantPriceRangeDto();
        variantPriceRangeDto2.setReductionHistory(new ArrayList<>());

        ProductDto fullpriceProductDto = new ProductDto();
        fullpriceProductDto.setProductId(productId);
        fullpriceProductDto.setTitle(title);
        fullpriceProductDto.setColorSwatches(colorSwatchDtos);
        fullpriceProductDto.setVariantPriceRange(variantPriceRangeDto2);

        return List.of(discountedProductDto, fullpriceProductDto);
    }

}
