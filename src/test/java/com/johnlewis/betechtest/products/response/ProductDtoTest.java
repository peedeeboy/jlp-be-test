package com.johnlewis.betechtest.products.response;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductDtoTest {

    @Test
    void shouldBuildProduct() {
        String productId = "productId";
        String title = "title";
        VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto();
        variantPriceRangeDto.setDisplay(new Display("10.00", "5.00"));
        ReductionHistoryDto reductionHistory = new ReductionHistoryDto();
        reductionHistory.setDisplay(new Display("15.00", "10.00"));
        reductionHistory.setChronology(0);
        variantPriceRangeDto.setReductionHistory(List.of(reductionHistory));
        ColorSwatchDto colorSwatchDto = new ColorSwatchDto();
        colorSwatchDto.setBasicColor("Blue");
        colorSwatchDto.setSkuId("skuId");
        List<ColorSwatchDto> colorSwatchDtos = List.of(colorSwatchDto);

        ProductDto productDto = new ProductDto();
        productDto.setProductId(productId);
        productDto.setTitle(title);
        productDto.setColorSwatches(colorSwatchDtos);
        productDto.setVariantPriceRange(variantPriceRangeDto);

        assertEquals(productId, productDto.getProductId());
        assertEquals(title, productDto.getTitle());
        assertEquals(variantPriceRangeDto, productDto.getVariantPriceRange());
        assertEquals(colorSwatchDtos, colorSwatchDtos);
    }
}
