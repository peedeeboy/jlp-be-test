package com.johnlewis.betechtest.products.response;

import com.johnlewis.betechtest.products.request.LabelType;
import com.johnlewis.betechtest.products.util.ColorMapper;
import com.johnlewis.betechtest.products.util.PriceStringFormatter;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductTest {

    @Test
    void shouldBuildProduct() {
        String productId = "productId";
        String title = "title";
        String nowPrice = "£9.00";
        String priceLabel = PriceStringFormatter.formatLabelPrice(LabelType.SHOWWASNOW, "£12.00", null, "£9.00");
        ColorSwatch colorSwatch = new ColorSwatch("Blue", ColorMapper.getRgbValueForColor("Blue"), "skuId");
        List<ColorSwatch> colorSwatches = List.of(colorSwatch);

        Product product = new Product();
        product.setProductId(productId);
        product.setTitle(title);
        product.setColorSwatches(colorSwatches);
        product.setNowPrice(nowPrice);
        product.setPriceLabel(priceLabel);

        assertEquals(productId, product.getProductId());
        assertEquals(title, product.getTitle());
        assertEquals(colorSwatches, product.getColorSwatches());
        assertEquals(nowPrice, product.getNowPrice());
        assertEquals(priceLabel, product.getPriceLabel());
    }

}
