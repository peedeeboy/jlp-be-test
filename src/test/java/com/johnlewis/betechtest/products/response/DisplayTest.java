package com.johnlewis.betechtest.products.response;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DisplayTest {

    @Test
    void shouldBuildDisplayObject() {
        Display display = new Display();
        display.setMax("£15.00");
        display.setMin("£14.00");

        assertEquals("£15.00", display.getMax());
        assertEquals("£14.00", display.getMin());
    }
}
