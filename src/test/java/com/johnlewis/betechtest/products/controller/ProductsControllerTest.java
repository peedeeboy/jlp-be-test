package com.johnlewis.betechtest.products.controller;

import com.johnlewis.betechtest.products.request.LabelType;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.service.DressesService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class ProductsControllerTest {

    private final DressesService dressesService = mock(DressesService.class);
    private final ProductsController productsController = new ProductsController(dressesService, LabelType.SHOWWASNOW);

    @AfterEach
    void after(){
        verifyNoMoreInteractions(dressesService);
    }

    @Test
    void whenLabelTypeIsPassedAsParameter_thenCorrectEnumPassedToDressService(){
        LabelType labelType = LabelType.SHOWPERCDISCOUNT;
        Optional<LabelType> optionalParam = Optional.of(labelType);

        ResponseEntity<List<Product>> result = productsController.getDiscountDresses(optionalParam);
        verify(dressesService).getDiscountedDresses(labelType);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void whenEmptyIsPassedAsParameter_thenDefaultEnumPassedToDressService() {
        LabelType labelType = LabelType.SHOWWASNOW;

        ResponseEntity<List<Product>> result = productsController.getDiscountDresses(Optional.empty());
        verify(dressesService).getDiscountedDresses(labelType);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}