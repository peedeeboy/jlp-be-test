package com.johnlewis.betechtest.products.util;

import com.johnlewis.betechtest.products.request.LabelType;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PriceStringFormatterTest {

    @Test
    void whenPriceIsNull_thenZeroPoundsZeroPenceIsReturned() {
        assertEquals("£0.00", PriceStringFormatter.formatPrice(null));
    }

    @Test
    void whenPriceLessThanTenPounds_thenFormattedPriceIncludesPence() {
        assertEquals("£9.99", PriceStringFormatter.formatPrice("£9.99"));
    }

    @Test
    void whenCalculatingAFiftyPercentDiscount_thenCorrectDiscountPercentageStringIsReturned() {
        assertEquals("50%", PriceStringFormatter.calculatePercentageDiscount("£9.00", "£4.50"));
    }

    @Test
    void whenCalculatingZeroPercentDiscount_thenCorrectDiscountPercentageStringIsReturned() {
        assertEquals("0%", PriceStringFormatter.calculatePercentageDiscount("£10.00", "£10.00"));
    }

    @Test
    void whenCalculatingTwentyFivePercentDiscount_thenCorrectDiscountPercentageIsReturned() {
        assertEquals("25%", PriceStringFormatter.calculatePercentageDiscount("£10.00", "£7.50"));
    }

    @Test
    void whenCurrentPriceIsGreaterThanOriginalPrice_thenDiscountPercentageShouldBeZero() {
        assertEquals("0%", PriceStringFormatter.calculatePercentageDiscount("£7.00", "£10.50"));
    }

    @Test
    void whenLabelTypeIsShowWasNow_thenPriceLabelIsReturnedInCorrectFormat() {
        assertEquals("Was £12, now £9.50",
                PriceStringFormatter.formatLabelPrice(LabelType.SHOWWASNOW, "£12.00", null, "£9.50"));
    }

    @Test
    void whenLabelTypeIsShowWasThenNowAndThenIsNull_thenPriceLabelIsReturnedInCorrectFormat() {
        assertEquals("Was £12, now £9.50",
                PriceStringFormatter.formatLabelPrice(LabelType.SHOWWASTHENNOW, "£12.00", null, "£9.50"));
    }

    @Test
    void whenLabelTypeIsShowWasThenNowAndThenIsNotNull_thenPriceLabelIsReturnedInCorrectFormat() {
        assertEquals("Was £12, then £10, now £9.50",
                PriceStringFormatter.formatLabelPrice(LabelType.SHOWWASTHENNOW, "£12.00", "£10.00", "£9.50"));
    }

    @Test
    void whenLabelTypeIsPercDiscount_thenPriceLabelIsReturnedInCorrectFormat() {
        assertEquals("25% off - now £9.00",
                PriceStringFormatter.formatLabelPrice(LabelType.SHOWPERCDISCOUNT, "£12.00", null, "£9.00"));
    }

}
