package com.johnlewis.betechtest.products.util;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ColorMapperTest {

    @Test
    void whenInvalidBasicColorIsPassedAsParam_thenEmptyStringIsReturned() {
        assertEquals("", ColorMapper.getRgbValueForColor("Indigo"));
    }

    @Test
    void whenNullIsPassedAsParam_thenEmptyStringIsReturned() {
        assertEquals("", ColorMapper.getRgbValueForColor(null));
    }

    @Test
    void whenValidBasicColorIsPassedAsParam_thenCorrectRgbValueIsReturned() {
        assertEquals("FFFFFF", ColorMapper.getRgbValueForColor("White"));
    }
}
