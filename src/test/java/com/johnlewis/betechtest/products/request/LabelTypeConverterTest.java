package com.johnlewis.betechtest.products.request;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.ConverterRegistry;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LabelTypeConverterTest {

    @Value("${jl.request.default.labeltype}")
    private LabelType DEFAULT_LABELTYPE;
    @Autowired
    ConversionService conversionService;
    @Autowired
    LabelTypeConverter labelTypeConverter;
    @Autowired
    private ConverterRegistry converterRegistry;

    @BeforeAll
    void setup() {
        converterRegistry.addConverter(labelTypeConverter);
    }

    @Test
    void whenLabelTypeIsRecognised_thenCorrectEnumValueIsReturned() {
        LabelType convertedLabelType = conversionService.convert("SHOWWASNOW", LabelType.class);
        assertEquals(LabelType.SHOWWASNOW, convertedLabelType);
    }

    @Test
    void whenLabelTypeIsValidButCaseIsWrong_thenCorrectEnumIsStillReturned() {
        LabelType convertedLabelType = conversionService.convert("SHOWpercDiscount", LabelType.class);
        assertEquals(LabelType.SHOWPERCDISCOUNT, convertedLabelType);
    }

    @Test
    void whenLabelTypeIsInvalid_thenDefaultLabelTypeEnumIsReturned() {
        LabelType convertedLabelType = conversionService.convert("XXXXXX", LabelType.class);
        assertEquals(DEFAULT_LABELTYPE, convertedLabelType);
    }

}
